## Telegram Go Bot

This is a simple telegram bot made in Kotlin to be able to play Go Games in telegram, in private chats and in group chats.
It implements the latest version of GNU Go AI for a basic Player vs AI

For the moment it is a very basic implementation that have all the minimum requirements for a game of go, but can't estimate score mid game nor detects dead groups.

## Usage

Search in telegram for @Go_Table_Bot and start playing go!
You can also add it to groups to play with other people, but the bot will ned the permission to read all messages.

## Future Work

 - Introduce score estmation
 - Dead group detection
 - Online play without a group

## Contribute

Just send a Pull request!
