package xyz.anbal.Server.Notifications.Telegram

class Formation private constructor(val stones: List<Stone>, val table: MutableList<MutableList<String>>) {

    val isAlive: Boolean
        get() = stones.any { it.freedoms > 0 }

    companion object {
        fun getFormation(stone: Stone, table: MutableList<MutableList<String>>): Formation {
            val toExpand = mutableListOf(stone)
            val expandedFormation = mutableListOf<Stone>()

            // Expand the formation connected to this stone

            while (toExpand.isNotEmpty()) {
                val toAdd = mutableListOf<Stone>()
                for (point in toExpand) {
                    if (try { table[point.row-1+1][point.column-1] }catch (e: IndexOutOfBoundsException){ null } == stone.stoneType.colorString &&
                        expandedFormation.none { it.row == point.row+1 && it.column == point.column } &&
                        toExpand.none { it.row == point.row+1 && it.column == point.column } &&
                        toAdd.none { it.row == point.row+1 && it.column == point.column })

                        toAdd.add(Stone(point.row+1, point.column, stone.stoneType, table))
                    if (try { table[point.row-1-1][point.column-1] }catch (e: IndexOutOfBoundsException){ null } == stone.stoneType.colorString &&
                        expandedFormation.none { it.row == point.row-1 && it.column == point.column } &&
                        toExpand.none { it.row == point.row-1 && it.column == point.column } &&
                        toAdd.none { it.row == point.row-1 && it.column == point.column })

                        toAdd.add(Stone(point.row-1, point.column, stone.stoneType, table))
                    if (try { table[point.row-1][point.column-1+1] }catch (e: IndexOutOfBoundsException){ null } == stone.stoneType.colorString &&
                        expandedFormation.none { it.row == point.row && it.column == point.column+1 } &&
                        toExpand.none { it.row == point.row && it.column == point.column+1 } &&
                        toAdd.none { it.row == point.row && it.column == point.column+1 })

                        toAdd.add(Stone(point.row, point.column+1, stone.stoneType, table))
                    if (try { table[point.row-1][point.column-1-1] }catch (e: IndexOutOfBoundsException){ null } == stone.stoneType.colorString &&
                        expandedFormation.none { it.row == point.row && it.column == point.column-1 } &&
                        toExpand.none { it.row == point.row && it.column == point.column-1 } &&
                        toAdd.none { it.row == point.row && it.column == point.column-1 })

                        toAdd.add(Stone(point.row, point.column-1, stone.stoneType, table))
                }
                expandedFormation.addAll(toExpand)
                toExpand.clear()
                toExpand.addAll(toAdd)
            }
            return Formation(expandedFormation, table)
        }
    }
}