package xyz.anbal.Server.Notifications.Telegram

import com.beust.klaxon.Json
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.PrintWriter
import java.lang.Thread.interrupted
import java.lang.Thread.sleep
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread

/**
 * Wrapper to use GNU Go AI in 1vsAI
 *
 * https://www.gnu.org/software/gnugo/gnugo_19.html#SEC200
 */
class GNUGoPlayer (
    val boardSize: Int,
    val humanColor: StoneTypes,
    val level: Int,
    val komi: Float = 6.5f,
    val handicap: Int = 0
) {
    @Json(ignored = true)
    val columnDict = "ABCDEFGHJKLMNOPQRSTUVWXYZ"  // In GNUGo columns are letters
    @Json(ignored = true)
    val br: BufferedReader
    @Json(ignored = true)
    val pw: PrintWriter
    @Json(ignored = true)
    val gnugoColor: StoneTypes
        get() {
            return if (humanColor == StoneTypes.WHITE)
                StoneTypes.BLACK
            else
                StoneTypes.WHITE
        }

    init {
        val executableName =
                if (System.getProperty("os.name").contains("windows", ignoreCase = true))
                    "gnugo-win/gnugo.exe "
                else
                    "gnugo "
        val launchCommand =
            executableName +
            "--mode gtp " +
            "--quiet " +
            "--boardsize $boardSize " +
            "--handicap $handicap " +
            "--color ${humanColor.colorString.toLowerCase()} " +
            "--komi $komi " +
            "--level $level"

        val pair = launchCommand.runCommand(File("."))
        br = pair.first!!
        pw = pair.second!!
    }

    fun addStone(row: Int, column: Int, color: StoneTypes = humanColor): Boolean {
        if (color == StoneTypes.EMPTY)
            return true
        val movePosition = "${columnDict[column-1]}${(boardSize+1)-row-1}"
        val id = Random().nextInt(10)
        pw.println("$id play ${color.name.toLowerCase()} $movePosition")
        println("$id play ${color.name.toLowerCase()} $movePosition")
        val response = br.readResponse(id)
        if (response.contains("illegal", ignoreCase = true))
            return false
        return true
    }

    /**
     * Coordinates from gnugo play, or null if PASS
     */
    fun gnugoPlay(color: StoneTypes = gnugoColor): Pair<Int?, Int?> {
        val id = Random().nextInt(10)
        pw.println("$id genmove ${color.name.toLowerCase()}")
        println("$id genmove ${color.name.toLowerCase()}")
        val response = br.readResponse(id)
        val row: Int?
        val column: Int?
        if (response != "PASS") {
            row = response.substring(1).toInt()
            column = columnDict.indexOf(response[0])+1
        }else {
            row = null
            column = null
        }
        return Pair(row, column)
    }

    fun setHandicapStones(): MutableList<Pair<Int, Int>>? {
        if (handicap > 0) {
            val id = Random().nextInt(10)
            pw.println("$id fixed_handicap $handicap")
            println("$id fixed_handicap $handicap")
            while (!br.ready()) {
                sleep(100)
            }
            val response = br.readResponse(id).split(" ")
            val coords = mutableListOf<Pair<Int, Int>>()
            for (coord in response) {
                val row = response[1].toString().toInt()
                val column = columnDict.indexOf(response[0])+1
                coords.add(Pair(row, column))
            }
            return coords
        }
        return null
    }

    fun clearBoard() {
        val id = Random().nextInt(10)
        pw.println("$id clear_board")
        println("$id clear_board")
        br.readResponse(id)
    }

    fun quit() {
        pw.println("quit")
    }

    fun printBoard() {
        pw.println("5 showboard")
        var response = ""
        while (!response.startsWith("=5") && !response.startsWith("?5")) {
            response = br.readLine()
        }
        for (i in 0..boardSize+2)
            println(br.readLine())
    }

    /**
     * Executes given command on given directory and return a Pair with a BufferedReader and it's PrintWriter
     */
    private fun String.runCommand(workingDir: File): Pair<BufferedReader?, PrintWriter?> {
        return try {
            val parts = this.split("\\s".toRegex())
            val proc = ProcessBuilder(*parts.toTypedArray())
                .directory(workingDir)
                .redirectOutput(ProcessBuilder.Redirect.PIPE)
                .redirectError(ProcessBuilder.Redirect.PIPE)
                .start()

            proc.waitFor(1, TimeUnit.SECONDS)
            Pair(proc.inputStream.bufferedReader(), PrintWriter(proc.outputStream, true))
        } catch(e: IOException) {
            e.printStackTrace()
            Pair(null, null)
        }
    }
    
    private fun BufferedReader.readResponse(id: Int): String {
        val t = thread (start = true, isDaemon = true) {
            try {
                sleep(10000)
                while (!interrupted()) {
                    println("A response isn't being read for more than 10s")
                    sleep(1000)
                }
            } catch (e: InterruptedException) {}
        }
        var response = ""
        while (!response.startsWith("=$id") && !response.startsWith("?$id")) {
            response = this.readLine()
            println("Read from GNU GO: $response")
        }
        t.interrupt()
        return response.replace("=$id", "").trim()
    }
}