package xyz.anbal.Server.Notifications.Telegram

import com.beust.klaxon.Json

class Game (
    val chatId: Long,
    var tableSize: TableSizes = TableSizes.X19,
    val komi: Float = 6.5f
) {

    var gnugo: GNUGoPlayer? = null

    var player1id: Long? = null
    var player2id: Long? = null
    var player1Name: String? = null
    var player2Name: String? = null
    var player1Points = 0.0
    var player2Points = komi

    var messageId: Long? = null

    var table: MutableList<MutableList<String>> = mutableListOf()
    var lastTableWhite: MutableList<MutableList<String>>? = null
    var lastTableBlack: MutableList<MutableList<String>>? = null

    var turn = StoneTypes.BLACK

    @Json(ignored = true)
    var passedTurns = 0
        private set(value) {
            field = value
            if (field > 2)
                games.remove(this)
        }

    fun empyTable() {
        table = mutableListOf()

        for ( i in 0 until tableSize.size)
            table.add(mutableListOf())

        table.forEach {
            for (i in 0 until tableSize.size)
                it.add(StoneTypes.EMPTY.colorString)
        }
    }

    fun changeTurn(passing: Boolean = false) {
        turn =
            if (turn == StoneTypes.BLACK)
                StoneTypes.WHITE
            else
                StoneTypes.BLACK
        if (passing)
            passedTurns++
        else
            passedTurns = 0
    }

    fun surrender() {
        passedTurns = 10
        games.remove(this)
        gnugo?.quit()
    }

    fun addStone(row: Int, column: Int, color: StoneTypes = turn, isAiOnly: Boolean = false): Boolean {
        when {
            row < 1 -> return false
            row > tableSize.size -> return false
            column < 1 -> return false
            column > tableSize.size -> return false
        }

        passedTurns = 0

        // Check if gnugo thinks it's a legal move
        if (gnugo != null && gnugo!!.humanColor == turn && !isAiOnly)
            if (!gnugo!!.addStone((tableSize.size)-row, column, color)) {
                println("GNU Go doesn't thinks that move was legal")
                return false
            } else {
                gnugo!!.printBoard()
            }

        // Check that it's not being played over an existing stone (Doesn't count stone removal)
        if (Stone.getStone(row, column, table)?.stoneType != StoneTypes.EMPTY && color != StoneTypes.EMPTY) {
            println("You can't set a stone over another existing stone")
            return false
        }

        table[(tableSize.size+1)-row-1][column-1] = color.colorString

        printTable()

        // After adding a stone, check for stones without freedoms (row, column). Will only remove stones from the other player
        for (i in 1..tableSize.size)
            for (j in 1..tableSize.size)
                if (Stone.getStone(i, j, table)?.stoneType != turn &&
                    !Formation.getFormation(Stone.getStone(i, j, table)!!, table).isAlive)
                    Formation.getFormation(Stone.getStone(i, j, table)!!, table).stones.forEach { stone ->
                        table[(tableSize.size+1)-stone.row-1][stone.column-1] = StoneTypes.EMPTY.colorString

                        if (stone.stoneType == StoneTypes.BLACK)
                            player1Points++
                        else if (stone.stoneType == StoneTypes.WHITE)
                            player2Points++
                    }

        // To check the suicidal move, we have to check twice the freedoms of the formations, but the second time, we also kill the turn's stones without adding score
        for (i in 1..table.size)
            for (j in 1..table.size)
                if (!Formation.getFormation(Stone.getStone(i, j, table)!!, table).isAlive)
                    Formation.getFormation(Stone.getStone(i, j, table)!!, table).stones.forEach { stone ->
                        table[(tableSize.size+1)-stone.row-1][stone.column-1] = StoneTypes.EMPTY.colorString
                        println("That move was suicidal")
                        return false
                    }

        // Simple prevention of KO
        when (color) {
            StoneTypes.WHITE ->  {
                return if (table.compareStates(lastTableWhite)) {
                    table = lastTableBlack!!
                    println("KO illegal move")
                    false
                } else {
                    lastTableWhite = table.copy()
                    true
                }
            }
            StoneTypes.BLACK -> {
                return if (table.compareStates(lastTableWhite)) {
                    table = lastTableWhite!!
                    println("KO illegal move")
                    false
                } else {
                    lastTableBlack = table.copy()
                    true
                }
            }
            StoneTypes.EMPTY -> return true
        }
    }

    fun removeStone(stone: Stone) {
        addStone(stone.row, stone.column, StoneTypes.EMPTY)
        if (gnugo != null) {
            gnugo!!.clearBoard()
            for (i in 0 until tableSize.size)
                for (j in 0 until tableSize.size)
                    gnugo!!.addStone(i+1, j+1, StoneTypes.valueOf(table[i][j].toUpperCase()))
        }
    }

    /**
     * Counts the territories that belongs for each player, first=player1 second=player2
     */
    fun countPoints(): Pair<Int, Int> {
        val countedTerritories = mutableListOf<Stone>()
        var player1Territories = 0
        var player2Territories = 0

        for (i in 1..tableSize.size)
            for (j in 1..tableSize.size)
                if (Stone.getStone(i, j, table)?.stoneType == StoneTypes.EMPTY && countedTerritories.none { it.row == i && it.column == j }) {
                    val expandedTerritory = Formation.getFormation(Stone.getStone(i, j, table)!!, table).stones
                    countedTerritories.addAll(expandedTerritory)

                    // Check if the expanded territory is totally surrounded by the same StoneType
                    var stoneAround: StoneTypes? = StoneTypes.EMPTY
                    for (point in expandedTerritory) {
                        var type = StoneTypes.EMPTY
                        if (Stone.getStone(point.row+1, point.column, table)?.stoneType ?: StoneTypes.EMPTY != StoneTypes.EMPTY)
                            type = Stone.getStone(point.row+1, point.column, table)?.stoneType ?: StoneTypes.EMPTY
                        if (Stone.getStone(point.row-1, point.column, table)?.stoneType ?: StoneTypes.EMPTY != StoneTypes.EMPTY)
                            type = Stone.getStone(point.row-1, point.column, table)?.stoneType ?: StoneTypes.EMPTY
                        if (Stone.getStone(point.row, point.column+1, table)?.stoneType ?: StoneTypes.EMPTY != StoneTypes.EMPTY)
                            type = Stone.getStone(point.row, point.column+1, table)?.stoneType ?: StoneTypes.EMPTY
                        if (Stone.getStone(point.row, point.column-1, table)?.stoneType ?: StoneTypes.EMPTY != StoneTypes.EMPTY)
                            type = Stone.getStone(point.row, point.column-1, table)?.stoneType ?: StoneTypes.EMPTY

                        if (stoneAround == StoneTypes.EMPTY)
                            stoneAround = type
                        else if (stoneAround != type && type != StoneTypes.EMPTY)
                            stoneAround = null
                    }

                    if (stoneAround == StoneTypes.BLACK)
                        player1Territories += expandedTerritory.size
                    else if (stoneAround == StoneTypes.WHITE)
                        player2Territories += expandedTerritory.size
                }
        return Pair(player1Territories, player2Territories)
    }

    fun copyTableToBoard() {
        if (gnugo != null)
            for (i in 1..tableSize.size)
                for (j in 1..tableSize.size)
                    if (Stone.getStone(i, j, table)?.stoneType != StoneTypes.EMPTY)
                        gnugo!!.addStone(i, j, Stone.getStone(i, j, table)!!.stoneType)
    }

    fun printTable() {
        print("XX ")
        for (i in 1 .. tableSize.size)
            print(if (i<10) " 0$i" else " $i")
        println()
        for (i in 0 until tableSize.size) {
            print("${if (i<10) "${(tableSize.size-i)} " else "0${(tableSize.size-i)} "} ")
            for (j in 0 until tableSize.size)
                print(if (table[i][j][0] == 'E') " . " else " " + table[i][j][0] + " ")
            println()
        }
    }

    companion object {
        @Json(ignored = true)
        val games = mutableListOf<Game>()
    }
}

operator fun StoneTypes.not(): StoneTypes {
    return when {
        this == StoneTypes.WHITE -> StoneTypes.BLACK
        this == StoneTypes.BLACK -> StoneTypes.WHITE
        else -> StoneTypes.EMPTY
    }
}

fun MutableList<MutableList<String>>.compareStates(otherState: MutableList<MutableList<String>>?): Boolean {
    if (otherState == null || this.size != otherState.size)
        return false

    for (y in 0 until this.size)
        for (x in 0 until this.size)
            if (this[y][x] != otherState[y][x])
                return false

    return true
}

fun MutableList<MutableList<String>>.copy(): MutableList<MutableList<String>> {
    var newTable: MutableList<MutableList<String>> = mutableListOf()
    for ( i in 0 until this.size)
        newTable.add(mutableListOf())

    for (y in 0 until newTable.size)
        for (x in 0 until newTable.size)
            newTable[y].add(this[y][x])

    return newTable
}
