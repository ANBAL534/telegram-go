package xyz.anbal.Server.Notifications.Telegram

import java.io.File
import javax.imageio.ImageIO


object GoGraphics {
    val board19Img = File("resources/Board19.png")
    val board13Img = File("resources/Board13.png")
    val board9Img = File("resources/Board9.png")
    val blackImg = File("resources/Black45.png")
    val whiteImg = File("resources/White45.png")
    
    fun createTableImage(table: MutableList<MutableList<String>>): File {
        val gTable =
            when {
                table.size == 9 -> ImageIO.read(board9Img)
                table.size == 13 -> ImageIO.read(board13Img)
                else -> ImageIO.read(board19Img)
            }
        val g = gTable.createGraphics()

        val blackImage = ImageIO.read(blackImg)
        val whiteImage = ImageIO.read(whiteImg)

        for (i in 0 until table.size)
            for (j in  0 until table[i].size)
                if (table[i][j] == StoneTypes.BLACK.colorString)
                    g.drawImage(blackImage, j+(j.toDouble()*46.5).toInt(), i+(i.toDouble()*46.5).toInt(), null)
                else if (table[i][j] == StoneTypes.WHITE.colorString)
                    g.drawImage(whiteImage, j+(j.toDouble()*46.5).toInt(), i+(i.toDouble()*46.5).toInt(), null)

        val file = File("tmp/${table.hashCode()}.png")
        ImageIO.write(gTable, "png", file)
        return file
    }
}