package xyz.anbal.Server.Notifications.Telegram

import com.beust.klaxon.Klaxon
import java.io.File
import java.util.concurrent.Semaphore

fun main (args: Array<String>) {
    Runtime.getRuntime().addShutdownHook(ShutdownHook())
    println("Loading saved games...")
    File("saves/").list().forEach {
        // Load saved games
        val path = "saves/$it"
        val file = File(path).readText()
        val game = Klaxon().parse<Game>(file)
        Game.games.add(game!!)
        game.copyTableToBoard()
    }
    TelegramBot.startBot()
    println("Telegram bot started. --> tg://resolve?domain=Go_Table_Bot      @Go_Table_Bot")
    val lock = Semaphore(1)
    lock.acquire(2)
}