package xyz.anbal.Server.Notifications.Telegram

import com.beust.klaxon.Klaxon
import java.io.File

internal class ShutdownHook : Thread() {
    override fun run() {
        println("Closing GNU Go Instances...")
        for (game in Game.games)
            game.gnugo?.quit()

        File("saves/").deleteRecursively()
        File("saves/").mkdir()
        println("Saving current games...")
        Game.games.forEach {
            File("saves/${it.hashCode()}.save").writeText(Klaxon().toJsonString(it))
        }
    }
}