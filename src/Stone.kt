package xyz.anbal.Server.Notifications.Telegram

data class Stone (val row: Int, val column: Int, val stoneType: StoneTypes, val table: MutableList<MutableList<String>>) {
    val freedoms: Int
        get() {
            var freedoms = 0
            if (try { table[(table.size+1)-row-1+1][column-1] }catch (e: IndexOutOfBoundsException){ (!stoneType).colorString } == StoneTypes.EMPTY.colorString)
                freedoms++
            if (try { table[(table.size+1)-row-1-1][column-1] }catch (e: IndexOutOfBoundsException){ (!stoneType).colorString } == StoneTypes.EMPTY.colorString)
                freedoms++
            if (try { table[(table.size+1)-row-1][column-1+1] }catch (e: IndexOutOfBoundsException){ (!stoneType).colorString } == StoneTypes.EMPTY.colorString)
                freedoms++
            if (try { table[(table.size+1)-row-1][column-1-1] }catch (e: IndexOutOfBoundsException){ (!stoneType).colorString } == StoneTypes.EMPTY.colorString)
                freedoms++
            return freedoms
        }

    companion object {
        fun getStone(row: Int, column: Int, table: MutableList<MutableList<String>>): Stone? {
            val type = try { StoneTypes.valueOf(table[(table.size+1)-row-1][column-1].toUpperCase()) }catch (e: IndexOutOfBoundsException){ null }
                ?: return null
            return Stone(row, column, type, table)
        }
    }
}