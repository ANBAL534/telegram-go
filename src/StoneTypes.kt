package xyz.anbal.Server.Notifications.Telegram

enum class StoneTypes(val colorString: String) {
    BLACK("Black"),
    WHITE("White"),
    EMPTY("Empty")
}