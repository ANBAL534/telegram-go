package xyz.anbal.Server.Notifications.Telegram

enum class TableSizes(val size: Int) {
    X19(19),
    X13(13),
    X9(9)
}