package xyz.anbal.Server.Notifications.Telegram

import me.ivmg.telegram.Bot
import me.ivmg.telegram.bot
import me.ivmg.telegram.dispatch
import me.ivmg.telegram.dispatcher.callbackQuery
import me.ivmg.telegram.dispatcher.command
import me.ivmg.telegram.dispatcher.text
import me.ivmg.telegram.entities.CallbackQuery
import me.ivmg.telegram.entities.InlineKeyboardButton
import me.ivmg.telegram.entities.InlineKeyboardMarkup
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import java.lang.Thread.sleep
import kotlin.concurrent.thread

object TelegramBot {
    var botThread: Thread? = null
        private set(value) {
            if ( field == null )
                field = value
        }
    var bot: Bot? = null
        private set(value) {
            if (value != null)
                field = value
        }

    private const val helpMessage =
                """
                    Availables commands:
                        /newgame - Starts a new Go game.
                        /surrender - Surrenders from current game.
                        /pass - Pass turn from current game.
                        /changeTurn - Changes the turn (Stone color).
                        /print - Send again the board image.
                        /points - Compute current score.

                        1-19 - Way to declare movements in a game.
                        18 5 - Another way of declaring movements in a game.
                        * Add "rem" to the end of a stone position declaration to remove a stone from the game *
                """

    /**
     * Will start telegram bot main thread.
     * If called multiple times, it will only start the bot the first time it is called, other calls are ignored
     */
    fun startBot() {
        if (botThread != null)
            return
        botThread = thread(name = "Telegram Bot Main Thread", isDaemon = true, start = true) {
            bot = bot {
                logLevel = HttpLoggingInterceptor.Level.NONE
                timeout = 30
                token = "832225310:AAFPdZzL5zbIXQcUYbZAUrOciXZeKflYRaw"
                // tg://resolve?domain=Go_Table_Bot            @Go_Table_Bot

                dispatch {
                    command("start") { bot, update ->
                        bot.sendMessage(chatId = update.message!!.chat.id, text = helpMessage.trimIndent())
                    }
                    command("newgame") { bot, update ->
                        val chatId = update.message!!.chat.id
                        val fromId = update.message!!.from!!.id
                        if (Game.games.none { it.chatId == chatId && it.player2id == null } && Game.games.none { it.chatId == chatId && (it.player1id == fromId || it.player2id == fromId) } ) {
                            val inlineKeyboardMarkup = InlineKeyboardMarkup(generateSizeButtons())
                            bot.sendMessage(chatId = chatId, text = "Select table size", replyMarkup = inlineKeyboardMarkup)
                        } else {
                            bot.sendMessage(chatId = chatId, text = "There is another game waiting for a player in this chat or you are already in a game")
                        }
                    }
                    command("surrender") { bot, update ->
                        val chatId = update.message?.chat?.id ?: return@command
                        val fromId = update.message!!.from!!.id
                        val game = Game.games.find { it.chatId == chatId && (it.player2id == fromId || it.player1id == fromId) }
                        if (game == null) {
                            bot.sendMessage(chatId = chatId, text = "You are not in a game")
                        } else {
                            bot.sendMessage(chatId = chatId, text = "${game.turn.name} has surrended and the game ended")
                            game.surrender()
                        }
                    }
                    command("pass") { bot, update ->
                        val chatId = update.message?.chat?.id ?: return@command
                        val fromId = update.message!!.from!!.id
                        val game = Game.games.find { it.chatId == chatId && (it.player2id == fromId || it.player1id == fromId) }
                        if (game == null) {
                            bot.sendMessage(chatId = chatId, text = "You are not in a game")
                        } else {
                            game.changeTurn(true)
                            bot.sendMessage(chatId = chatId, text = "${game.turn.name} has passed ${game.passedTurns}/2")
                            if(game.passedTurns == 2){
                                bot.sendMessage(chatId = chatId, text = "Both players have passed, remove dead stones.\nEx. 11-4 rem\nUse /pass to end the game definitely. Or keep playing.")
                            } else if (game.passedTurns > 2) {
                                val territoryCount = game.countPoints()
                                val player1Total = territoryCount.first + game.player1Points
                                val player2Total = territoryCount.second + game.player2Points
                                bot.sendMessage(chatId = chatId, text =
                                "The game ended as both sides have passed and removed dead stones.\n" +
                                "BLACK Points: $player1Total\n" +
                                "WHITE Points: $player2Total (With 6.5 Komi)")
                                Game.games.remove(game)
                                game.gnugo?.quit()
                            }
                        }
                    }
                    command("changeturn") { bot, update ->
                        val chatId = update.message?.chat?.id ?: return@command
                        val fromId = update.message!!.from!!.id
                        val game = Game.games.find { it.chatId == chatId && (it.player2id == fromId || it.player1id == fromId) }
                        if (game == null) {
                            bot.sendMessage(chatId = chatId, text = "You are not in a game")
                        } else {
                            game.changeTurn()
                            bot.sendMessage(chatId = chatId, text = "Turn for ${game.turn.name}")

                            if (game.gnugo?.gnugoColor == game.turn)
                                aiMove(game, bot, chatId)
                        }
                    }
                    command("print") { bot, update ->
                        val chatId = update.message?.chat?.id ?: return@command
                        val fromId = update.message!!.from!!.id
                        val game = Game.games.find { it.chatId == chatId && (it.player2id == fromId || it.player1id == fromId) }
                        if (game == null) {
                            bot.sendMessage(chatId = chatId, text = "You are not in a game")
                        } else {
                            bot.sendPhoto(chatId, GoGraphics.createTableImage(game.table), caption = "Turn for ${game.turn.name}")
                            File("tmp").deleteRecursively()
                            File("tmp").mkdir()
                        }
                    }
                    command("points") {bot, update ->
                        val chatId = update.message?.chat?.id ?: return@command
                        val fromId = update.message!!.from!!.id
                        val game = Game.games.find { it.chatId == chatId && (it.player2id == fromId || it.player1id == fromId) }
                        if (game == null) {
                            bot.sendMessage(chatId = chatId, text = "You are not in a game")
                        } else {
                            val territoryCount = game.countPoints()
                            val player1Total = territoryCount.first + game.player1Points
                            val player2Total = territoryCount.second + game.player2Points
                            bot.sendMessage(chatId = chatId, text =
                            "Current points: \n" +
                            "BLACK Points: $player1Total\n" +
                            "WHITE Points: $player2Total (With 6.5 Komi)")
                        }
                    }
                    command("help") {bot, update ->
                        val chatId = update.message?.chat?.id ?: return@command
                        bot.sendMessage(chatId = chatId, text = helpMessage.trimIndent())
                    }
                    command("tables") { bot, update ->
                        val chatId = update.message?.chat?.id ?: return@command
                        val fromId = update.message!!.from!!.id
                        val game = Game.games.find { it.chatId == chatId && (it.player2id == fromId || it.player1id == fromId) }
                        if (game == null) {
                            bot.sendMessage(chatId = chatId, text = "You are not in a game")
                        } else {
                            game.printTable()
                            println()
                            println()
                            game.gnugo?.printBoard()
                        }
                    }

                    // Note: in callbackQuery, parameter data filters by a contains, it's not exact, does not difference case
                    callbackQuery("19x19") { bot, update ->
                        update.callbackQuery?.let {
                            val chatId = it.message!!.chat.id
                            val fromId = it.from.id
                            if (Game.games.find { it.chatId == chatId && (it.player2id == fromId || it.player1id == fromId) } != null) {
                                // If already in a game, delete board selection message
                                bot.deleteMessage(chatId, it.message!!.messageId)
                                return@callbackQuery
                            }
                            val game = Game(chatId, TableSizes.X19)
                            game.empyTable()
                            game.player1id = fromId
                            game.player1Name = "@"+it.from.username
                            Game.games.add(game)
                            waitOpponent(it, bot)
                        }
                    }
                    callbackQuery("13x13") { bot, update ->
                        update.callbackQuery?.let {
                            val chatId = it.message!!.chat.id
                            val fromId = it.from.id
                            if (Game.games.find { it.chatId == chatId && (it.player2id == fromId || it.player1id == fromId) } != null) {
                                // If already in a game, delete board selection message
                                bot.deleteMessage(chatId, it.message!!.messageId)
                                return@callbackQuery
                            }
                            val game = Game(chatId, TableSizes.X13)
                            game.empyTable()
                            game.player1id = fromId
                            game.player1Name = "@"+it.from.username
                            Game.games.add(game)
                            waitOpponent(it, bot)
                        }
                    }
                    callbackQuery("9x9") { bot, update ->
                        update.callbackQuery?.let {
                            val chatId = it.message!!.chat.id
                            val fromId = it.from.id
                            if (Game.games.find { it.chatId == chatId && (it.player2id == fromId || it.player1id == fromId) } != null) {
                                // If already in a game, delete board selection message
                                bot.deleteMessage(chatId, it.message!!.messageId)
                                return@callbackQuery
                            }
                            val game = Game(chatId, TableSizes.X9)
                            game.empyTable()
                            game.player1id = fromId
                            game.player1Name = "@"+it.from.username
                            Game.games.add(game)
                            waitOpponent(it, bot)
                        }
                    }
                    callbackQuery("player2") { bot, update ->
                        update.callbackQuery?.let {
                            val chatId = it.message!!.chat.id
                            if (Game.games.none { it.chatId == chatId && it.player2id == null }) {
                                bot.sendMessage(chatId = chatId, text = "There are no other games waiting for players, start a new one using /newgame")
                            } else {
                                val game = Game.games.find { it.chatId == chatId && it.player2id == null }!!
                                game.player2id = it.from.id
                                game.player2Name = "@"+it.from.username
                                game.messageId = it.message!!.messageId

                                bot.editMessageText(chatId, it.message!!.messageId, text = "Let a battle begin between ${game.player1Name} and ${game.player2Name}\nFirst is blacks.\nWrite the position you want your stone in row-column (Ex. 11-8 or 11 8)")
                                bot.sendPhoto(chatId, GoGraphics.createTableImage(game.table))
                                File("tmp").deleteRecursively()
                                File("tmp").mkdir()
                            }
                        }
                    }
                    callbackQuery("gnugocolor") { bot, update ->
                        update.callbackQuery?.let {
                            val chatId = it.message!!.chat.id
                            val inlineKeyboardMarkup = InlineKeyboardMarkup(
                                listOf(
                                    listOf(
                                        InlineKeyboardButton(
                                            text = "Black",
                                            callbackData = "gnugolvl-b"
                                        )
                                    ),
                                    listOf(
                                        InlineKeyboardButton(
                                            text = "White",
                                            callbackData = "gnugolvl-w"
                                        )
                                    )
                                )
                            )
                            bot.editMessageText(chatId, it.message!!.messageId,
                                text = "Select AI Color",
                                replyMarkup = inlineKeyboardMarkup
                            )
                        }
                    }
                    callbackQuery("gnugolvl") { bot, update ->
                        update.callbackQuery?.let {
                            val chatId = it.message!!.chat.id
                            val aiColor = it.data.replace("gnugolvl-", "")
                            val inlineKeyboardMarkup = InlineKeyboardMarkup(
                                listOf(
                                    listOf(
                                        InlineKeyboardButton(
                                            text = "1",
                                            callbackData = "level-1-$aiColor"
                                        )
                                    ),
                                    listOf(
                                        InlineKeyboardButton(
                                            text = "2",
                                            callbackData = "level-2-$aiColor"
                                        )
                                    ),
                                    listOf(
                                        InlineKeyboardButton(
                                            text = "3",
                                            callbackData = "level-3-$aiColor"
                                        )
                                    ),
                                    listOf(
                                        InlineKeyboardButton(
                                            text = "4",
                                            callbackData = "level-4-$aiColor"
                                        )
                                    ),
                                    listOf(
                                        InlineKeyboardButton(
                                            text = "5",
                                            callbackData = "level-5-$aiColor"
                                        )
                                    ),
                                    listOf(
                                        InlineKeyboardButton(
                                            text = "6",
                                            callbackData = "level-6-$aiColor"
                                        )
                                    ),
                                    listOf(
                                        InlineKeyboardButton(
                                            text = "7",
                                            callbackData = "level-7-$aiColor"
                                        )
                                    ),
                                    listOf(
                                        InlineKeyboardButton(
                                            text = "8",
                                            callbackData = "level-8-$aiColor"
                                        )
                                    ),
                                    listOf(
                                        InlineKeyboardButton(
                                            text = "9",
                                            callbackData = "level-9-$aiColor"
                                        )
                                    ),
                                    listOf(
                                        InlineKeyboardButton(
                                            text = "10",
                                            callbackData = "level-10-$aiColor"
                                        )
                                    )
                                )
                            )
                            bot.editMessageText(chatId, it.message!!.messageId,
                                text = "Select AI Level from 1 to 10",
                                replyMarkup = inlineKeyboardMarkup
                            )
                        }
                    }
                    callbackQuery("level-") { bot, update ->
                        update.callbackQuery?.let {
                            val chatId = it.message!!.chat.id
                            val color = it.data.replace("level-\\d+-".toRegex(), "")
                            val level = it.data.replace("level-", "").replace("-[bw]".toRegex(), "").toInt()
                            val game = Game.games.find { it.chatId == chatId && it.player2id == null }!!
                            game.player2id = -1
                            game.player2Name = "GNU GO level $level"
                            game.messageId = it.message!!.messageId

                            val humanStoneType =
                                if (color == "w")
                                    StoneTypes.BLACK
                                else
                                    StoneTypes.WHITE

                            game.gnugo = GNUGoPlayer(game.tableSize.size, humanStoneType, level)

                            bot.editMessageText(chatId, it.message!!.messageId, text = "Let a battle begin between ${game.player1Name} and ${game.player2Name}\nFirst is blacks.\nWrite the position you want your stone in (Ex. 11-8)")
                            bot.sendPhoto(chatId, GoGraphics.createTableImage(game.table))
                            File("tmp").deleteRecursively()
                            File("tmp").mkdir()

                            if (color == "b")
                                aiMove(game, bot, chatId)
                        }
                    }
                    callbackQuery("showoff") { bot, update ->
                        update.callbackQuery?.let {
                            val chatId = it.message!!.chat.id
                            if (Game.games.none { it.chatId == chatId && it.player2id == null }) {
                                bot.sendMessage(chatId = chatId, text = "There are no other games waiting for players, start a new one using /newgame")
                            } else {
                                val game = Game.games.find { it.chatId == chatId && it.player2id == null }!!
                                game.player1id = -1
                                game.player2id = -1
                                game.player1Name = "GNU Go 1"
                                game.player2Name = "GNU Go 2"
                                game.messageId = it.message!!.messageId
                                game.gnugo = GNUGoPlayer(game.tableSize.size, StoneTypes.BLACK, 10)

                                bot.editMessageText(chatId, it.message!!.messageId, text = "Let a battle begin between ${game.player1Name} and ${game.player2Name}\nFirst is blacks.\nLet the AIs battle, will try to play once every 10s")
                                bot.sendPhoto(chatId, GoGraphics.createTableImage(game.table))
                                File("tmp").deleteRecursively()
                                File("tmp").mkdir()

                                return@let thread (start = true, isDaemon = true, name = "Bot-battle") {
                                    while (Game.games.contains(game)) {
                                        try {
                                            sleep(10000)
                                            aiMove(game, bot, chatId, StoneTypes.BLACK, true)
                                            sleep(10000)
                                            aiMove(game, bot, chatId, StoneTypes.WHITE, true)
                                        } catch (e: IllegalStateException) {
                                            break  // Game ended as the reader tried to read a null reader
                                        }
                                    }
                                }
                            }
                        }
                    }

                    text { bot, update ->
                        val chatId = update.message?.chat?.id ?: return@text
                        val text = update.message!!.text!!
                        val fromId = update.message!!.from!!.id

                        when {
                            text.contains("^\\d+-\\d+$".toRegex()) -> {
                                try {
                                    val split = text.split("-")
                                    val row = split[0].toInt()
                                    val column = split[1].toInt()
                                    thread(start = true, isDaemon = true) { makeMove(row, column, chatId, fromId, bot) }
                                } catch (e: Exception) {}
                                return@text
                            }
                            text.contains("^\\d+\\s\\d+$".toRegex()) -> {
                                try {
                                    val split = text.split(" ")
                                    val row = split[0].toInt()
                                    val column = split[1].toInt()
                                    thread(start = true, isDaemon = true) { makeMove(row, column, chatId, fromId, bot) }
                                } catch (e: Exception) {}
                                return@text
                            }
                            text.contains("\\d+-\\d+".toRegex()) && text.contains("rem", ignoreCase = true) -> {
                                try {
                                    val text = text.replace("rem", "").trim()
                                    val split = text.split("-")
                                    val row = split[0].toInt()
                                    val column = split[1].toInt()
                                    thread(start = true, isDaemon = true) { makeMove(row, column, chatId, fromId, bot, true) }
                                } catch (e: Exception) {}
                                return@text
                            }
                            text.contains("\\d+\\s\\d+".toRegex()) && text.contains("rem", ignoreCase = true) -> {
                                try {
                                    val text = text.replace("rem", "").trim()
                                    val split = text.split(" ")
                                    val row = split[0].toInt()
                                    val column = split[1].toInt()
                                    thread(start = true, isDaemon = true) { makeMove(row, column, chatId, fromId, bot, true) }
                                } catch (e: Exception) {}
                                return@text
                            }
                        }
                    }
                }
            }
            bot!!.startPolling()
        }
    }

    private fun makeMove(
        row: Int,
        column: Int,
        chatId: Long,
        fromId: Long,
        bot: Bot,
        remove: Boolean = false
    ) {
        val game = Game.games.find { it.chatId == chatId && (it.player2id == fromId || it.player1id == fromId) }!!
        if (!remove) {
            if (!game.addStone(row, column)) {
                bot.sendPhoto(chatId, File("resources/illegal.png"), caption = "Illegal move")
                println("Wanted move r=$row c=$column was invalid")
                return
            }
            game.changeTurn()
        } else {
            game.removeStone(Stone.getStone(row, column, game.table) ?: return)
        }

        bot.sendPhoto(chatId, GoGraphics.createTableImage(game.table), caption =
        "Turn for ${game.turn.name}\n" +
        " - Black has captured ${game.player1Points.toInt()} stones.\n" +
        " - White has captured ${(game.player2Points-game.komi).toInt()} stones.")
        File("tmp").deleteRecursively()
        File("tmp").mkdir()

        sleep(2500)

        // Play gnugo if selected
        if (game.gnugo != null && game.passedTurns < 2)
            aiMove(game, bot, chatId)
    }

    private fun waitOpponent(it: CallbackQuery, bot: Bot) {
        val chatId = it.message?.chat?.id ?: return
        val game = Game.games.find { game ->
            game.chatId == chatId && game.player1id == it.from.id
        } ?: return  // Do nothing if the OC didn't select the option
        bot.deleteMessage(chatId, it.message!!.messageId)

        val inlineKeyboardMarkup = InlineKeyboardMarkup(
            listOf(
                listOf(
                    InlineKeyboardButton(
                        text = "Become the Opponent",
                        callbackData = "player2"
                    )
                ),
                listOf(
                    InlineKeyboardButton(
                        text = "Play against GNU GO",
                        callbackData = "gnugocolor"
                    )
                ),
                listOf(
                    InlineKeyboardButton(
                        text = "AI vs AI",
                        callbackData = "showoff"
                    )
                )
            )
        )
        bot.sendMessage(chatId = chatId, text = "Waiting for a second player", replyMarkup = inlineKeyboardMarkup)
    }

    private fun aiMove(game: Game, bot: Bot, chatId: Long, color: StoneTypes = game.gnugo!!.gnugoColor, isAiOnly: Boolean = false) {
        // Play bot turn if turned on
        if (game.gnugo != null) {
            bot.sendMessage(chatId = chatId, text = "Wait, I'm thinking about a master move!")
            val aiMove = game.gnugo!!.gnugoPlay(color)
            if (aiMove.first == null || aiMove.second == null) {  // AI Passed
                game.changeTurn(true)
                bot.sendMessage(chatId = chatId, text = "${game.turn.name} has passed ${game.passedTurns}/2")
                if(game.passedTurns == 2){
                    bot.sendMessage(chatId = chatId, text = "Both players have passed, remove dead stones.\nEx. 11-4 rem\nUse /pass to end the game definitely. Or keep playing.")
                } else if (game.passedTurns > 2) {
                    val territoryCount = game.countPoints()
                    val player1Total = territoryCount.first + game.player1Points
                    val player2Total = territoryCount.second + game.player2Points
                    bot.sendMessage(chatId = chatId, text =
                    "The game ended as both sides have passed and removed dead stones.\n" +
                            "BLACK Points: $player1Total\n" +
                            "WHITE Points: $player2Total (With 6.5 Komi)")
                    Game.games.remove(game)
                    game.gnugo!!.quit()
                }
            } else {  // AI played a move
                bot.sendMessage(chatId = chatId, text = "GNU Go: ${aiMove.first}-${aiMove.second}")

                game.addStone(aiMove.first!!, aiMove.second!!, color, isAiOnly)
                game.changeTurn()

                bot.sendPhoto(chatId, GoGraphics.createTableImage(game.table), caption =
                "Turn for ${game.turn.name}\n" +
                " - Black has captured ${game.player1Points.toInt()} stones.\n" +
                " - White has captured ${(game.player2Points-game.komi).toInt()} stones.")
                File("tmp").deleteRecursively()
                File("tmp").mkdir()
            }
        }
    }
}

fun generateSizeButtons(): List<List<InlineKeyboardButton>> {
    return listOf(
        listOf(InlineKeyboardButton(text = "19x19", callbackData = "19x19")),
        listOf(InlineKeyboardButton(text = "13x13", callbackData = "13x13")),
        listOf(InlineKeyboardButton(text = "9x9", callbackData = "9x9"))
    )
}